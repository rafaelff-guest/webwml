#use wml::debian::cdimage title="Fungeren als spiegelserver voor Debian-cd-images" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="a2762a4686797f8bd714431c6bc466b37657847b"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<p>Om een spiegelserver voor Debian-cd-images te hebben, moet u een Linux- of
Unix-machine hebben met een permanente, betrouwbare verbinding met het Internet.
Debian-cd-spiegelservers hebben <tt>.iso</tt>-images voor cd's en dvd's van
verschillende groottes, bestanden voor
<a href="https://www.einval.com/~steve/software/jigdo/">jigdo</a> (<tt>.jigdo</tt> en <tt>.template</tt>),
<a href="https://nl.wikipedia.org/wiki/BitTorrent">BitTorrent</a>-bestanden
(<tt>.torrent</tt>) en verificatiebestanden voor de images
(<tt>SHA512SUMS*</tt> en <tt>SHA256SUMS*</tt>).</p>

<toc-display/>

#______________________________________________________________________________

<toc-add-entry name="master">Hoofdsite</toc-add-entry>

<p><!-- Er zijn twee lokaties voor spiegeling, één voor stabiele images en één voor
bèta/onstabiele/testing-images. --> De URL's van de hoofdsite worden hieronder
weergegeven - echter, overweeg <strong>alstublieft</strong> om te spiegelen van
een naburige spiegelserver (spiegelserverlijsten:
<a href="../http-ftp/">HTTP/FTP</a>, <a href="rsync-mirrors">rsync</a>) indien
mogelijk. Toegang tot de hoofdsite kan worden beperkt rond releasetijd.</p>

<p>Denk er ook aan dat een <strong>enorme</strong> hoeveelheid data opgeslagen
wordt in deze mappen - lees de <a href="#exclude">onderstaande rubriek</a>
voor informatie over hoe u die kunt verkleinen door bepaalde bestanden
uit te sluiten.</p>

<ul>

  <li>Stabiele images (bijgewerkt voor elke stabiele release):<br>

    <a href="https://cdimage.debian.org/debian-cd/"
    ><tt>https://cdimage.debian.org/debian-cd/</tt></a><br>

    <tt>rsync://cdimage.debian.org/debian-cd/</tt>

  </li>

  <li>Wekelijkse images:<br>

    <a href="https://cdimage.debian.org/cdimage/weekly-builds/"
    ><tt>https://cdimage.debian.org/cdimage/weekly-builds/</tt></a><br>

    <tt>rsync://cdimage.debian.org/cdimage/weekly-builds/</tt>

  </li>

  <li>Dagelijkse images:<br>

    <a href="https://cdimage.debian.org/cdimage/daily-builds/"
    ><tt>https://cdimage.debian.org/cdimage/daily-builds/</tt></a><br>

    <tt>rsync://cdimage.debian.org/cdimage/daily-builds/</tt>

  </li>

</ul>
#______________________________________________________________________________

<toc-add-entry name="httpftp">Spiegelservers die HTTP/FTP gebruiken worden
afgeraden</toc-add-entry>

<p>U zou geen FTP of HTTP mogen gebruiken om uw spiegelserver bij te werken.
Omdat de bestanden enorm groot zijn, is het erg waarschijnlijk
dat deze transfermethodes mislukken.</p>

<p>Bovendien hebben HTTP en FTP geen integriteitscontroles op de afgehaalde
data. Dit maakt het waarschijnlijker dat afgebroken afhalingen of datacorruptie
niet opgemerkt worden.</p>
#______________________________________________________________________________

<toc-add-entry name="rsync">Spiegelservers die rsync gebruiken zijn
aanvaardbaar</toc-add-entry>

<p>Het programma <a href="https://rsync.samba.org/"><kbd>rsync</kbd></a> is een
goede oplossing voor een spiegelserver. Het is minder efficiënt dan de andere,
Debian-specifieke spiegelserveroplossing hieronder, maar het is misschien
gemakkelijker op te zetten. Bovendien zorgt het ervoor dat alle bestanden
correct worden opgehaald en dat de metadata (bijv. tijdstempels)
gesynchroniseerd blijven net als de bestandsdata.</p>

<p>Zie de sectie <a href="#exclude">Bestanden uitsluiten voor spiegeling</a>
voor voorbeelden van <kbd>--include</kbd>- en <kbd>--exclude</kbd>-schakelaars.
De <a href="rsync-mirrors">lijst van rsync-spiegelservers</a> is beschikbaar
op een aparte pagina.</p>

<p>Gebruik ten minste de opties <strong><kbd>--times --links --hard-links --partial
--block-size=8192</kbd></strong>. Dit zal de datum van wijziging, symbolische
en harde koppelingen behouden en een blokgrootte van 8192 bytes gebruiken
(efficiëntst voor CD-images). Wanneer de datum van wijziging en de grootte
hetzelfde zijn, zal <kbd>rsync</kbd> de bestanden ongemoeid laten, dus
<kbd>--times</kbd> is echt nodig.</p>
#______________________________________________________________________________

<toc-add-entry name="jigdolite">Spiegelservers die gebruik maken van jigdo-lite
worden afgeraden</toc-add-entry>

<p>Recente versies van het
programma <a href="https://www.einval.com/~steve/software/jigdo/"><kbd>jigdo-lite</kbd></a>
ondersteunen bij het afhalen batchverwerking van meerdere images. We raden
echter niet aan om <kbd>jigdo-lite</kbd> te gebruiken om Debian-CD-spiegelservers
aan te maken - gebruik liever <kbd>jigdo-mirror</kbd>.</p>
#______________________________________________________________________________

<toc-add-entry name="jigdomirror">Spiegelservers die gebruik maken van jigdo-mirror worden aanbevolen</toc-add-entry>

<p>Eigenlijk betekent dit: spiegel de <tt>.iso</tt>-bestanden gebruikmakend van
<a href="https://www.einval.com/~steve/software/jigdo/"><kbd>jigdo-mirror</kbd></a>, en
(als u ook andere types van bestanden wilt spiegelen, bijv. <tt>.jigdo</tt>- en
<tt>.template</tt>-bestanden) voer daarna rsync uit op de map om de rest af te
halen. De scripts op
<a href="http://www.acc.umu.se/~maswan/debian-push/cdimage/">deze pagina</a>
kunnen u helpen om dit op te zetten.</p>

<p>Veel mensen beheren "gewone" Debian-spiegelservers (<kbd>debian/</kbd>),
of hebben zo'n spiegelserver in de buurt. Dit betekent dat ze de <tt>.deb</tt>'s
die zich in de cd/dvd-images bevinden, al hebben. De voor de hand liggende vraag
is -- waarom kunnen we deze bestanden niet gebruiken voor de cd/dvd-images?</p>

<p><kbd>jigdo-mirror</kbd> is een programma dat toelaat om een set Debian-cd/dvd-images
te genereren, gebruikmakend van de bestanden van een "normale" spiegelserver,
plus een aantal extra jigdo-templatebestanden.</p>

<p>Vooreerst hebt u de jigdo-templatebestanden nodig. Zie
<a href="../jigdo-cd">de jigdo-informatiepagina</a> voor links.
Download de bestanden voor elke architectuur waarvoor u images wilt bouwen.</p>

<p>Maak het <kbd>~/.jigdo-mirror</kbd>-bestand aan om het programma in te stellen.
Dit is een voorbeeld:</p>

<pre>
jigdoDir="/daar/bewaar/ik/mirrors/debian-cd/current/jigdo"
imageDir="/daar/bewaar/ik/mirrors/debian-cd/current/images"
tmpDir="/daar/bewaar/ik/mirrors/debian-cd/current/images"
debianMirror="file:/daar/bewaar/ik/mirrors/debian"
include='i386/|sparc/|powerpc/|source/'; exclude='-1\.'
</pre>

<p>De variabelen <i>include</i> en <i>exclude</i> bevatten de lijst van
architecturen waarvoor u images wilt maken (reguliere expressies, eigenlijk).
Voor meer informatie, zie de <kbd>jigdo-mirror</kbd>-manpagina of de broncode
(het is een shell-script met veel commentaar).</p>

<p>Nadat u het heeft ingesteld, voert u <kbd>jigdo-mirror</kbd> uit en het zal
alles automatisch doen. Het zal veel uitvoer genereren en waarschijnlijk een
tijdje in beslag nemen. We suggereren dus dat u voorzieningen treft om daarmee
om te gaan (voer het uit in screen, leg de uitvoer om naar een bestand, enz.).</p>
#______________________________________________________________________________

<toc-add-entry name="pushmirror">Hoe een duwende spiegelserver worden?</toc-add-entry>

<p>Wanneer er nieuwe images beschikbaar zijn, kan de hoofdsite een boodschap
sturen naar haar spiegelservers en ze onmiddellijk laten starten met het
bijwerken. Op die manier wordt de nieuwe data eerder "geduwd" naar in plaats
van "binnengetrokken" door de spiegelservers bij hun eerstvolgende
dagelijkse bijwerking, wat leidt tot snellere doorstroming van nieuwe
releases van images.</p>

<p>Als u uw spiegelserver wilt laten deelnemen aan dit bijwerkingssysteem,
bekijk dan <a href="http://www.acc.umu.se/~maswan/debian-push/cdimage/">
deze pagina</a>.</p>
#______________________________________________________________________________

<toc-add-entry name="exclude">Bestanden uitsluiten van spiegeling</toc-add-entry>

<p>Om de opslagruimte die uw Debian-cd-spiegelserver nodig heeft, te beperken,
kunt u bepaalde bestanden uitsluiten van spiegeling. De volgende instructies bevatten
commandoregelschakelaars voor <kbd>rsync</kbd>, maar kunnen u ook helpen als u
andere hulpmiddelen gebruikt voor de spiegeling. Met <kbd>rsync</kbd>, wordt
met <kbd>--include</kbd>- en <kbd>--exclude</kbd>-schakelaars in volgorde
van voorkomen rekening gehouden. De eerste schakelaar waarvan het bestandspatroon
een overeenkomst oplevert, bepaalt of het bestand wordt uitgesloten of opgenomen.</p>

<ul>

  <li><strong>Broncode uitsluiten:</strong>
  <kbd>--exclude=source/</kbd><br/>

  Dit zal vermijden dat images die broncode bevatten, worden gespiegeld. Merk
  op dat sommige mensen het ongehoord vinden om programma's onder GPL-licensie
  aan te bieden op een server zonder ook de broncode aan te bieden <em>op
  dezelfde server</em>.</li>

  <li><strong>Volledige images uitsluiten:</strong>
  <kbd>--include='*netinst*.iso'
  --exclude='*.iso'</kbd><br/>

  Sluit voor elke architectuur alle complete sets cd/dvd-images uit,
  <em>maar</em> spiegelt de net-install-<tt>.iso</tt>-images. We raden aan
  om deze kleine images altijd te spiegelen: ten opzichte van hun
  grootte zijn ze uitermate nuttig!</li>

  <li><strong>Volledige images voor niet-i386-architecturen uitsluiten:</strong>
  <kbd>--include '*netinst*.iso'
  --include='i386/**.iso' --exclude='*.iso'</kbd><br/>

  Zoals hierboven, maar voeg <em>wel</em> alle cd/dvd-images toe voor de
  i386-architectuur.</li>

  <li><strong>Sluit volledige images uit, behalve de i386 cd's 1 tot 3:</strong>
  <kbd>--include='*netinst*.iso' --include='i386/**-[1-3].iso'
  --exclude='*.iso'</kbd><br/>

  De volledige set van i386-images neemt voor u misschien nog te veel plaats in
  als hij ook dvd- en tweelagige dvd-images omvat. Dit sluit alle
  <tt>.iso</tt>-images uit behalve de net-install-images en de eerste drie
  i386-dvd's.</li>

  <li><strong>Sluit verschillende architecturen uit behalve i386:</strong>
  <kbd>--exclude=alpha/ --exclude=arm/ --exclude=hppa/ --exclude=hurd/
  --exclude=ia64/ --exclude=m68k/ --exclude=mips/ --exclude=mipsel/
  --exclude=powerpc/ --exclude=s390/ --exclude=sh/
  --exclude=sparc/</kbd><br/>

  Voeg enkel de volledige set van bestanden voor i386 toe, sluit alle
  <tt>.jigdo</tt>, <tt>.iso</tt> en dergelijke bestanden uit voor de andere
  architecturen.<br>

  <strong>Controleer de lijst van architecturen vooraleer u begint te spiegelen
 - de lijst wijzigt en deze voorbeelden zijn mogelijk verouderd!</strong></li>

</ul>
#______________________________________________________________________________

<toc-add-entry name="names">Naamconventies en plaatsvereisten voor <tt>.iso</tt>-images</toc-add-entry>

<p>De verschillende varianten <tt>.iso</tt>-images worden onderscheiden
door hun namen, wat u toelaat bepaalde types images voor spiegeling uit te
sluiten:</p>

<ul>

  <li><strong><tt>*-netinst.iso</tt></strong>: één image voor elke
  architectuur, tot maximum 500&nbsp;MB</li>

  <li><strong><tt>*-dvd.iso</tt></strong> (enkellagige dvd's): meerdere
  images, elk tot 4482&nbsp;MB groot. Voor buster zijn er tot <strong>16</strong>
  dvd-images per architectuur. De servers van Debian
  bieden slechts een kleine subset dvd-images aan in .iso-formaat die direct
  gedownload kunnen worden: 3 voor amd64, 3 voor i386 en 1 voor andere
  architecturen. De overige images worden enkel in jigdo-vorm aangeboden.
  </li>

  <li><strong><tt>*-bd.iso</tt></strong> (enkellagige blu-rays): zoals
  hiervoor, behalve dat elk image tot 23&nbsp;GB groot is.
  Deze images zijn enkel beschikbaar als jigdo-bestand voor een beperkt
  aantal architecturen (amd64 en i386) en voor de broncode.</li>

  <li><strong><tt>*-dlbd.iso</tt></strong> (dubbellagige blu-rays): zoals
  hiervoor, behalve dat elk image tot 48&nbsp;GB groot is.
  Deze images zijn enkel beschikbaar als jigdo-bestand voor een beperkt
  aantal architecturen (amd64 en i386) en voor de broncode.</li>

  <li><strong><tt>*-STICK16GB*.iso</tt></strong> (16GB USB-images): zoals
  hiervoor, behalve dat elk image tot 16&nbsp;GB groot is.
  Deze images zijn enkel beschikbaar als jigdo-bestand voor een beperkt
  aantal architecturen (amd64 en i386).</li>

</ul>
#______________________________________________________________________________

<toc-add-entry name="register">De spiegelserver registreren</toc-add-entry>

<p>Om uw cd-imagespiegelserver nuttig te maken voor een breder publiek, kunt u
hem registreren in onze spiegelserverlijst zoals
<a href="../http-ftp/">deze</a> of <a href="rsync-mirrors">deze</a>.
Echter, omdat volledige images grote bestanden zijn, kan dit vele gigabytes
per dag aan netwerktrafiek tot gevolg hebben.</p>

<p>U kunt uw spiegelserver registreren door ofwel
<a href="$(HOME)/mirror/submit">het spiegelserveraanvraagformulier</a> in te
vullen (merk op dat de CDImage-* velden de belangrijke zijn), ofwel door een
e-mail te sturen naar
<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;debian-cd&#64;lists.debian.org">\
debian-cd&#64;lists.debian.org</a>.</p>

<p>We appreciëren alle nieuwe cd-imagespiegelservers. Dank bij voorbaat!</p>

