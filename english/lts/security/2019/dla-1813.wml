<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were found in PHP, a widely-used open source general
purpose scripting language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11039">CVE-2019-11039</a>

    <p>An integer underflow in the iconv module could be exploited to trigger
    an out of bounds read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11040">CVE-2019-11040</a>

    <p>A heap buffer overflow was discovered in the EXIF parsing code.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.6.40+dfsg-0+deb8u4.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1813.data"
# $Id: $
