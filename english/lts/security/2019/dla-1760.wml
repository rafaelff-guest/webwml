<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Kusano Kazuhiko discovered a buffer overflow vulnerability in the handling
of Internationalized Resource Identifiers (IRI) in wget, a network utility
to retrieve files from the web, which could result in the execution of
arbitrary code or denial of service when recursively downloading from an
untrusted server.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.16-1+deb8u6.</p>

<p>We recommend that you upgrade your wget packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1760.data"
# $Id: $
