<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been fixed in libsdl2, the newer version of the 
Simple DirectMedia Layer library that provides low level access to
audio, keyboard, mouse, joystick, and graphics hardware.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2888">CVE-2017-2888</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>

    <p>Potential overflow in surface allocation was fixed.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.0.5+dfsg1-2+deb9u2.</p>

<p>We recommend that you upgrade your libsdl2 packages.</p>

<p>For the detailed security status of libsdl2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libsdl2">https://security-tracker.debian.org/tracker/libsdl2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2803.data"
# $Id: $
