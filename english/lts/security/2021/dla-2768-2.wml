<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A regression was introduced in DLA-2768-1, where the uwsgi proxy
module for Apache2 (mod_proxy_uwsgi) interprets incorrect Apache
configurations in a less forgiving way, causing existing setups to
fail after upgrade.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.0.14+20161117-3+deb9u5.</p>

<p>We recommend that you upgrade your uwsgi packages.</p>

<p>For the detailed security status of uwsgi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/uwsgi">https://security-tracker.debian.org/tracker/uwsgi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2768-2.data"
# $Id: $
