<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in libspring-java, a modular
Java/J2EE application framework. An attacker may execute code, perform
XST attack, issue unauthorized cross-domain requests or cause a DoS
(Denial-of-Service) in specific configurations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1270">CVE-2018-1270</a>

    <p>Spring Framework allows applications to expose STOMP over
    WebSocket endpoints with a simple, in-memory STOMP broker through
    the spring-messaging module. A malicious user (or attacker) can
    craft a message to the broker that can lead to a remote code
    execution attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11039">CVE-2018-11039</a>

    <p>Spring Framework allows web applications to change the HTTP
    request method to any HTTP method (including TRACE) using the
    HiddenHttpMethodFilter in Spring MVC. If an application has a
    pre-existing XSS vulnerability, a malicious user (or attacker) can
    use this filter to escalate to an XST (Cross Site Tracing) attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11040">CVE-2018-11040</a>

    <p>Spring Framework allows web applications to enable cross-domain
    requests via JSONP (JSON with Padding) through
    AbstractJsonpResponseBodyAdvice for REST controllers and
    MappingJackson2JsonView for browser requests. Both are not enabled
    by default in Spring Framework nor Spring Boot, however, when
    MappingJackson2JsonView is configured in an application, JSONP
    support is automatically ready to use through the <q>jsonp</q> and
    <q>callback</q> JSONP parameters, enabling cross-domain requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15756">CVE-2018-15756</a>

    <p>Spring Framework provides support for range requests when serving
    static resources through the ResourceHttpRequestHandler, or
    starting in 5.0 when an annotated controller returns an
    org.springframework.core.io.Resource. A malicious user (or
    attacker) can add a range header with a high number of ranges, or
    with wide ranges that overlap, or both, for a denial of service
    attack.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.3.5-1+deb9u1.</p>

<p>We recommend that you upgrade your libspring-java packages.</p>

<p>For the detailed security status of libspring-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libspring-java">https://security-tracker.debian.org/tracker/libspring-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2635.data"
# $Id: $
