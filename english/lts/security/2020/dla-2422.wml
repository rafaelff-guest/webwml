<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Malformed SVG images were able to cause a segmentation fault
in qtsvg-opensource-src, the QtSvg module for displaying the
contents of SVG files in Qt.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.7.1~20161021-2.1.</p>

<p>We recommend that you upgrade your qtsvg-opensource-src packages.</p>

<p>For the detailed security status of qtsvg-opensource-src please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qtsvg-opensource-src">https://security-tracker.debian.org/tracker/qtsvg-opensource-src</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2422.data"
# $Id: $
