<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In ruby-json before 2.3.0, there is an unsafe object creation
vulnerability. When parsing certain JSON documents, the json
gem (including the one bundled with Ruby) can be coerced into
creating arbitrary objects in the target system.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.1-1+deb8u1.</p>

<p>We recommend that you upgrade your ruby-json packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2190.data"
# $Id: $
