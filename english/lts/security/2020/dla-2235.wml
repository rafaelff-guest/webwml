<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a file descriptor leak in the D-Bus message
bus.</p>

<p>An unprivileged local attacker could use this to attack the system DBus
daemon, leading to denial of service for all users of the machine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12049">CVE-2020-12049</a>

    <p>File descriptor leak in _dbus_read_socket_with_unix_fds</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.8.22-0+deb8u3.</p>

<p>We recommend that you upgrade your dbus packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2235.data"
# $Id: $
