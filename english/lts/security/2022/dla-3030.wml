<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue was found in zipios++, a small C++ library for reading zip files.
Due to wrong handling of malformed zip files, an infinite loop could be
entered, which results in a denial of service.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.1.5.9+cvs.2007.04.28-6+deb9u1.</p>

<p>We recommend that you upgrade your zipios++ packages.</p>

<p>For the detailed security status of zipios++ please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zipios++">https://security-tracker.debian.org/tracker/zipios++</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3030.data"
# $Id: $
