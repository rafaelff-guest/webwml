<define-tag pagetitle>Debian 10 <q>Buster</q> vydán</define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986" maintainer="Miroslav Kure"

<p>
Projekt Debian s potěšením oznamuje, že po 25 měsících vývoje byla
vydána nová stabilní verze 10, kódovým názvem <q>Buster</q>.
Díky spojenému úsilí týmů
<a href="https://security-team.debian.org/">Debian Security</a> a
<a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>
bude tato verze podporována 5 let.

<p>
Debian 10 <q>Buster</q> obsahuje nejrůznější destopové aplikace
a desktopová prostředí, mezi jinými například:
</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>
V tomto vydání používá GNOME jako výchozí zobrazovací server Wayland,
namísto tradičního Xorg. Wayland má jednodušší a modernější návrh, což
je dobré pro bezpečnost. Zobrazovací server Xorg je nicméně stále
instalován a uživatelé si mohou při výběru sezení zvolit, zda použijí
Wayland nebo Xorg.
</p>

<p>
Díky projektu Reproducible Builds vytváří přes 91% zdrojových balíků
v Debianu 10 bitově identické binární balíky. To je důležitá vlastnost,
která chrání uživatele před zákeřnými pokusy o úpravu kompilátorů
a kompilovacích farem. Příští vydání Debianu budou obsahovat nástroje
a metadata, pomocí kterých si mohou koncoví uživatelé ověřit původ
balíků v archivu.
</p>

<p>
Pro lepší zabezpečení se nyní automaticky instaluje a používá
AppArmor, což je systém pro mandatorní řízení přístupu, kterým lze
omezit přístupy jednotlivých programů k různým zdrojům. Dále všechny
způsoby získávání balíků pomocí APT (s výjimkou metod cdrom, gpgv
a rsh) mohou volitelně využívat filtrování <q>seccomp-BPF</q>. APT
nyní obsahuje metodu https přímo v balíku apt a není třeba ji
instalovat zvlášť.
</p>

<p>
V Debianu 10 <q>Buster</q> je filtrování sítě založeno na frameworku
nftables. Počínaje verzí 1.8.2 obsahuje binární balík iptables dvě
varianty příkazového rozhraní: iptables-nft a iptables-legacy, přičemž
varianta iptables-nft používá subsystém linuxového jádra nf_tables.
Pro přepínání mezi oběma variantami lze použít systém
<q>alternativ</q>.
</p>

<p>
Podpora UEFI (<q>Unified Extensible Firmware Interface</q>), která
byla poprvé představena v Debianu 7 <q>Wheezy</q>, byla v Debianu 10
<q>Buster</q> významně vylepšena. Toto vydání podporuje Secure Boot na
architekturách amd64, i386 a arm64, což by mělo bezproblémově fungovat
na většině počítačů, kde je toto rozšíření povoleno. To znamená, že
uživatelé již nemusí podporu Secure Bootu vypínat.
</p>

<p>
Debian 10 <q>Buster</q> ve výchozí instalaci obsahuje balíky cups
a cups-filters, které by měly zajistit vše, co je třeba pro
automatický tisk bez ovladačů. Fronty pro síťový tisk a IPP tiskárny
se automaticky nastaví pomocí cups-browsed a můžete tak zapomenout na
použití nesvobodných ovladačů jednotlivých výrobců.
</p>

<p>
Debian 10 <q>Buster</q> obsahuje množství aktualizovaných softwarových
balíků (přes 62% oproti předchozímu vydání), mezi kterými nechybí třeba:
</p>
<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (v balíku firefox-esr)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 a 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux řady 4.19</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>a více než 59 000 dalších softwarových balíků připravených
k okamžitému použití, sestavených z téměř 29 000 zdrojových
balíků.</li>
</ul>

<p>
S tímto bohatým výběrem softwaru a tradiční podporou širokého spektra
počítačových architektur Debian opět potvrzuje svůj záměr být
univerzálním operačním systémem. Je vhodný pro mnoho odlišných způsobů
nasazení: od desktopů k netbookům, od vývojářských stanic ke klastrům
a produkčním databázovým, webovým či datovým serverům. Aby
<q>Buster</q> splnil vysoká očekávání, která uživatelé stabilního
vydání předpokládají, byly použity dodatečné způsoby zajištění kvality
jako například automatické testování instalací a aktualizací všech
balíků v archivu.
</p>

<p>
Celkově je podporováno deset architektur:
64 bitové PC / Intel EM64T / x86-64 (<code>amd64</code>),
32 bitové PC / Intel IA-32 (<code>i386</code>),
64 bitové little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64 bitové IBM S/390 (<code>s390x</code>),
ARM (<code>armel</code> resp. <code>armhf</code> pro starší
resp. novější 32 bitový hardware a <code>arm64</code> pro
64 bitovou architekturu <q>AArch64</q>)
a MIPS (32 bitové architektury <code>mips</code>
(big-endian) a <code>mipsel</code> (little-endian) a
64 bitový little-endian hardware (<code>mips64el</code>)).
</p>

<h3>Chcete Debian vyzkoušet?</h3>

<p>
Chcete-li Debian 10 <q>Buster</q> vyzkoušet jednoduše bez nutnosti
cokoliv instalovat, můžete využít některý z
<a href="$(HOME)/CD/live/">živých (live) obrazů</a>, který zavede celý
operační systém do operační paměti počítače, kde si ho můžete
vyzkoušet bez obav z rozbití stávajícího systému.
</p>

<p>
Živé obrazy jsou k dispozici na architekturách <code>amd64</code>
a <code>i386</code> ve formě obrazů pro DVD, USB paměti a zavádění ze
sítě. Uživatel si může vyzkoušet různá desktopová prostředí: Cinnamon,
GNOME, KDE Plasma, LXDE, MATE, Xfce a nově i LXQt.  S Busterem se
navrací i standardní živý obraz základního Debianu bez grafického
uživatelského rozhraní.
</p>

<p>
Jestliže se vám bude nový operační systém líbit, máte možnost si ho
nainstalovat na disk počítače přímo z živého obrazu. Živé obrazy
obsahují jak standardní instalační systém Debianu, tak i nezávislý
instalátor Calamares. Více informací naleznete v
<a href="$(HOME)/releases/buster/releasenotes">poznámkách k vydání</a>
a na stránkách Debianu v části
<a href="$(HOME)/CD/live/">živé obrazy</a>.
</p>

<p>
Jestliže chcete Debian rovnou nainstalovat, můžete si vybrat
z nejrůznějších instalačních médií, jako jsou Blu-ray, DVD, CD, USB
paměti, nebo třeba síť. Při instalaci je možné volit z několika
desktopových prostředí &mdash; Cinnamon, GNOME, KDE Plasma Desktop
a Aplikace, LXDE, LXQt, MATE nebo Xfce. Navíc jsou k dispozici
<q>vícearchitekturní</q> CD a DVD, která umožňují instalaci na různých
architekturách z jediného média. Také si třeba můžete vytvořit
instalační USB klíčenku (více se dozvíte v <a
href="$(HOME)/releases/stretch/installmanual">instalační
příručce</a>).
</p>

<p>
Pro cloudové uživatele Debian nabízí přímou podporu mnoha známých
cloudových platforem. Oficiální obrazy s Debianem se dají jednoduše
zvolit z nabídky u konkrétních poskytovatelů. Debian také poskytuje
předpřipravené <code>amd64</code> a <code>arm64</code>
<a href="https://cloud.debian.org/images/openstack/current/">
obrazy pro OpenStack</a>, které lze použít pro lokální cloud.
</p>

<p>
Debian je nyní možné instalovat v 76 jazycích, z nichž většina funguje
jak v textovém, tak v grafickém rozhraní.
</p>

<p>
Obrazy instalačních médií si můžete již nyní stáhnout přes
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (doporučená možnost),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> nebo
<a href="$(HOME)/CD/http-ftp/">HTTP</a> (vizte
<a href="$(HOME)/CD/">Debian na CD</a>). V brzké době bude <q>Buster</q>
dostupný i u <a href="$(HOME)/CD/vendors">prodejců</a> na DVD, CD
a Blu-ray discích.
</p>


<h3>Aktualizace Debianu</h3>

<p>
Přechod na Debian 10 z předchozího vydání (Debian 9 <q>Stretch</q>) je
ve většině případů řešen automaticky nástrojem pro správu balíků
apt. Aktualizace Debianu může být jako vždy provedena bezbolestně,
na místě a bez zbytečné odstávky systému, ovšem důrazně doporučujeme
přečíst si
<a href="$(HOME)/releases/buster/releasenotes">poznámky k vydání</a> a
<a href="$(HOME)/releases/buster/installmanual">instalační příručku</a>
a předejít tak potenciálním problémům. V nejbližších týdnech po vydání
se budou poznámky k vydání dále vylepšovat a překládat do dalších
jazyků.
</p>


<h2>O Debianu</h2>

<p>
Debian je svobodný operační systém vyvíjený tisícovkami dobrovolníků
z celého světa spolupracujících prostřednictvím Internetu. Hlavními
silnými stránkami projektu Debian jsou dobrovolnická základna,
dodržování společenské smlouvy Debianu (Debian Social Contract)
a odhodlání poskytovat nejlepší možný operační systém. Vydání nové
verze Debianu je v tomto směru dalším důležitým krokem.
</p>


<h2>Kontaktní informace</h2>

<p>
Pro další informace prosím navštivte webové stránky Debianu na
<a href="$(HOME)/">https://www.debian.org/</a> nebo zašlete email na
adresu &lt;press@debian.org&gt;.
</p>
