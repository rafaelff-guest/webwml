#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été corrigés dans nagios3, un système de gestion et
de surveillance pour des hôtes, services et réseaux.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18245">CVE-2018-18245</a>

<p>Maximilian Boehner de usd AG a trouvé une vulnérabilité de script intersite
(XSS) dans Nagios Core. Cette vulnérabilité permet à des attaquants de placer du
code JavaScript malveillant dans l’interface web à travers une manipulation de
sortie de greffon. Pour réaliser cela, l’attaquant doit pouvoir manipuler la
sortie renvoyée par les vérifications de nagios, par exemple, en remplaçant un
greffon sur un des terminaux surveillés. L’exécution de la transmission de
données utiles nécessite alors qu’un utilisateur authentifié crée un rapport
d’alerte contenant la sortie correspondante.

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9566">CVE-2016-9566</a>

<p>Des utilisateurs locaux avec accès à un compte dans le groupe nagios peuvent
obtenir les droits du superutilisateur à l'aide d'un attaque par lien symbolique
sur le fichier de journal de débogage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-1878">CVE-2014-1878</a>

<p>Un problème a été corrigé qui permettait à des attaquants distants de
provoquer un dépassement de pile et subséquemment un déni de service (erreur de
segmentation) à l'aide d'un long message vers cmd.cgi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7205">CVE-2013-7205</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2013-7108">CVE-2013-7108</a>

<p>Un défaut a été corrigé dans Nagios qui pouvait être exploité pour provoquer
un déni de service. Cette vulnérabilité est provoquée par une erreur due à un
décalage d'entier dans la fonction process_cgivars(). Elle pourrait être
exploitée pour provoquer une lecture hors limites en envoyant une valeur
spécialement contrefaite de clef vers l’interface utilisateur web de Nagios.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 3.5.1.dfsg-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nagios3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1615.data"
# $Id: $
