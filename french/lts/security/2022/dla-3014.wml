#use wml::debian::translation-check translation="eaaedf7fcc5cd1bcf60f3890e52786a6b0d8231a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été signalée dans src:elog, un système de carnet de
bord pour gérer des notes à travers une interface Web. Cette vulnérabilité
permet à des attaquants distants de créer une condition de déni de service 
sur les installations de ELOG Electronic Logbook. Une authentification
n'est pas requise pour exploiter cette vulnérabilité. Ce défaut particulier
se trouve dans le traitement des paramètres HTTP. Une requête contrefaite
peut déclencher le déréférencement d'un pointeur NULL. Un attaquent peut
exploiter cette vulnérabilité pour créer une condition de déni de service.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
3.1.2-1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets elog.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de elog, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/elog">\
https://security-tracker.debian.org/tracker/elog</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3014.data"
# $Id: $
