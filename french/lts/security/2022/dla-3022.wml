#use wml::debian::translation-check translation="92abd96adb3903f62157c932948316453d5fe2f0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Max Justicz a signalé une vulnérabilité de traversée de répertoires dans
Dpkg::Source::Archive dans dpkg, le système de gestion de paquets de
Debian. Cela affecte l'extraction de paquets source non fiables dans les
formats de paquet source v2 et v3 qui incluent une archive debian.tar.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.18.26.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dpkg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de dpkg, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/dpkg">\
https://security-tracker.debian.org/tracker/dpkg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3022.data"
# $Id: $
