#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Clamav est vulnérable à plusieurs problèmes pouvant conduire à un déni de
service lors du traitement de contenu non fiable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6418">CVE-2017-6418</a>

<p>Lecture hors limites dans libclamav/message.c, permettant à des attaquants
distants de provoquer un déni de service à l'aide d'un message de courriel
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6420">CVE-2017-6420</a>

<p>Utilisation de mémoire après libération dans la fonction wwunpack
(libclamav/wwunpack.c) permettant à des attaquants distants de provoquer un
déni de service à l'aide d'un fichier PE contrefait avec compression WWPack.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.99.2+dfsg-0+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1105.data"
# $Id: $
