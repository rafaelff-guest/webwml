#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7482">CVE-2017-7482</a>

<p>Shi Lei a découvert que le code de traitement de ticket RxRPC de Kerberos 5
ne vérifie pas correctement les métadonnées, conduisant à une divulgation
d'informations, un déni de service ou éventuellement une exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7542">CVE-2017-7542</a>

<p>Une vulnérabilité de dépassement d’entier dans la fonction
ip6_find_1stfragopt() a été découverte permettant à un attaquant local avec
privilèges d’ouvrir des sockets brutes pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7889">CVE-2017-7889</a>

<p>Tommi Rantala et Brad Spengler ont signalé que le sous-système mm n’impose
pas correctement le mécanisme de protection CONFIG_STRICT_DEVMEM, permettant
à un attaquant local avec accès à /dev/mem d’obtenir des informations sensibles ou
éventuellement d’exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10661">CVE-2017-10661</a>

<p>Dmitry Vyukov de Google a signalé que la fonctionnalité timerfd ne gérait
pas correctement certaines opérations simultanées sur un descripteur unique
de fichier. Cela permet à un attaquant local de provoquer un déni de service
ou éventuellement d'exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10911">CVE-2017-10911</a> / XSA-216

<p>Anthony Perard de Citrix a découvert un défaut de fuite d'informations
dans le traitement des réponses blkif de Xen, permettant à un client
malveillant non privilégié d'obtenir des informations sensibles à partir de
l'hôte ou d'autres clients.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11176">CVE-2017-11176</a>

<p>La fonction mq_notify() ne règle pas le pointeur « sock » à NULL lors de
son entrée dans la logique de réessai. Un attaquant peut tirer avantage de
ce défaut lors de la fermeture de l'espace utilisateur d'un socket Netlink
pour provoquer un déni de service ou éventuellement d'autres impacts.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11600">CVE-2017-11600</a>

<p>Bo Zhang a signalé que le sous-système xfrm ne valide pas correctement un
des paramètres d'un message netlink. Des utilisateurs locaux dotés de la
capacité CAP_NET_ADMIN peuvent utiliser cela pour provoquer un déni de
service ou éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12134">CVE-2017-12134</a> / #866511 / XSA-229

<p>Jan H. Schoenherr d'Amazon a découvert que lorsque Linux est exécuté dans
un domaine de paravirtualisation (PV) Xen sur un système x86, il peut
fusionner incorrectement les requêtes de bloc d'entrée et de sortie. Un
client bogué ou malveillant peut déclencher le bogue dans dom0 ou un domaine
de paravirtualisation, provoquant un déni de service ou éventuellement
l'exécution de code arbitraire.</p>

<p>Ce problème peut être atténué en désactivant les fusions sur les
périphériques bloc dorsaux sous-jacents, par exemple :
<code>echo 2 > /sys/block/nvme0n1/queue/nomerges</code></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12153">CVE-2017-12153</a>

<p>Bo Zhang a signalé que le sous-système cfg80211 (wifi) ne validait pas
correctement les paramètres d'un message netlink. Des utilisateurs locaux
dotés de la capacité CAP_NET_ADMIN (dans n'importe quel espace de noms avec
un périphérique wifi) peuvent utiliser cela pour provoquer un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12154">CVE-2017-12154</a>

<p>Jim Mattson de Google a signalé que l'implémentation de KVM pour les
processeurs Intel x86 ne gérait pas correctement certaines configurations
d'hyperviseur imbriqué. Un client malveillant (ou un client imbriqué dans un
hyperviseur compatible L1) pourrait utiliser cela pour un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14106">CVE-2017-14106</a>

<p>Andrey Konovalov a découvert qu'une division par zéro déclenchable par
l'utilisateur dans la fonction tcp_disconnect() pourrait avoir pour
conséquence un déni de service local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14140">CVE-2017-14140</a>

<p>Otto Ebeling a signalé que l'appel système move_pages() réalisait une
validation insuffisante des identifiants des processus appelant et cible,
avec pour conséquence un contournement partiel d'ASLR. Cela facilite
l'exploitation par des utilisateurs locaux de vulnérabilités dans des
programmes installés avec le bit d'autorisation « set-UID » configuré.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14156">CVE-2017-14156</a>

<p><q>sohu0106</q> a signalé une fuite d'informations dans le pilote vidéo
atyfb. Un utilisateur local doté d'un accès à un périphérique de framebuffer
géré par ce pilote pourrait utiliser cela pour obtenir des informations
sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14340">CVE-2017-14340</a>

<p>Richard Wareing a découvert que l'implémentation de XFS permet la
création de fichiers avec l'indicateur <q>realtime</q> sur un système de
fichiers n'ayant aucun périphérique en temps réel, ce qui peut avoir pour
conséquence un plantage (oops). Un utilisateur local doté d'un accès à un
système de fichiers XFS qui n'a pas de périphérique en temps réel peut
utiliser cela pour un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14489">CVE-2017-14489</a>

<p>ChunYu Wang de Red Hat a découvert que le sous-système iSCSI ne valide
pas correctement la longueur d'un message netlink, menant à une corruption
de mémoire. Un utilisateur local avec les droits pour gérer les
périphériques iSCSI peut utiliser cela pour un déni de service ou
éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000111">CVE-2017-1000111</a>

<p>Andrey Konovalov de Google a signalé une situation de compétition dans la
fonctionnalité paquet brut (af_packet). Des utilisateurs locaux avec la
capacité CAP_NET_RAW peuvent utiliser cela pour un déni de service ou
éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000251">CVE-2017-1000251</a> / nº 875881

<p>Armis Labs a découvert que le sous-système Bluetooth ne validait pas
correctement les réponses de configuration L2CAP, menant à un dépassement de
tampon de pile. Cela est une des nombreuses vulnérabilités nommées
<q>Blueborne</q>. Un attaquant proche peut utiliser cela pour provoquer un
déni de service ou éventuellement pour exécuter du code arbitraire sur un
système où Bluetooth est activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000363">CVE-2017-1000363</a>

<p>Roee Hay a signalé que le pilote lp ne vérifie pas correctement les limites
des arguments passés. Cela n'a pas d'impact de sécurité impact dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000365">CVE-2017-1000365</a>

<p>Les pointeurs argument et environnement ne sont pas correctement pris en
compte par les restrictions de taille imposées aux chaînes d'argument et
d'environnement passées par RLIMIT_STACK/RLIMIT_INFINITY. Un attaquant local
peut tirer avantage de ce défaut en conjonction avec d'autres défauts pour
exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000380">CVE-2017-1000380</a>

<p>Alexander Potapenko de Google a signalé une situation de compétition dans
le pilote temporisateur d'ALSA (son), menant à une fuite d'informations. Un
utilisateur local doté des droits pour accéder aux périphériques de son
pourrait utiliser cela pour obtenir des informations sensibles.</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.2.93-1. Cette version inclut aussi des corrections de bogue
à partir de versions amont jusqu’à 3.2.93 compris.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.43-2+deb8u4 ou ont été corrigés dans une précédente version.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.9.30-2+deb9u4 ou ont été corrigés dans une précédente version.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1099.data"
# $Id: $
