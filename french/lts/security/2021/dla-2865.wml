#use wml::debian::translation-check translation="70b65e90997b8f40fe2d50faaa6f80449e574f94" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été corrigées dans la pile SIP reSIProcate.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11521">CVE-2017-11521</a>

<p>La fonction SdpContents::Session::Medium::parse permettait à des
attaquants distants de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12584">CVE-2018-12584</a>

<p>La fonction ConnectionBase::preparseNewBytes permettait à des attaquants
distants de provoquer un déni de service ou éventuellement d'exécuter du
code arbitraire lorsque la communication TLS est activée.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:1.11.0~beta1-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets resiprocate.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de resiprocate,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/resiprocate">\
https://security-tracker.debian.org/tracker/resiprocate</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2865.data"
# $Id: $
