#use wml::debian::translation-check translation="d58692868f11bdc7b603376107b7bd959dbac4df" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans edk2, un
micrologiciel pour des machines virtuelles. Des dépassements d’entier ou de pile
et une consommation de ressources incontrôlée pourraient conduire à un déni de
service ou, dans le pire des scénarios, permettre éventuellement à un
utilisateur local authentifié d’augmenter ses privilèges.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0~20161202.7bbe0b3e-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets edk2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de edk2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/edk2">\
https://security-tracker.debian.org/tracker/edk2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2645.data"
# $Id: $
