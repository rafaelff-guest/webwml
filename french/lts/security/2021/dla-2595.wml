#use wml::debian::translation-check translation="b967d3421b79b51ecabb192534f3211bef2a6286" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité d’exécution potentielle
de code arbitraire dans velocity, un moteur de patron basé sur Java pour écrire
des applications web. Elle pourrait être exploitée par des applications qui
permettent à des utilisateurs de téléverser ou de modifier des patrons.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13936">CVE-2020-13936</a>

<p>Un attaquant qui est capable de modifier des patrons de Velocity peut
exécuter du code Java arbitraire ou des commandes de système arbitraires avec
les mêmes privilèges que le compte exécutant le conteneur du servlet. Cela
s’applique aux applications qui autorisent les utilisateurs non authentifiés
à téléverser ou modifier des patrons velocity exécutant le moteur Velocity
d’Apache, jusqu’à y compris la version 2.2 .</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.7-5+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets velocity.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2595.data"
# $Id: $
