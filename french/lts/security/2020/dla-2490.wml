#use wml::debian::translation-check translation="7856bcb0b4b81033a2f18edb2e6dddd6fc04cf51" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Guenal Davalan a signalé un défaut dans x11vnc, un serveur VNC permettant
l'accès à distance à une session X existante. x11vnc crée des segments de
mémoire partagée en mode 0777. Un attaquant local peut tirer avantage de ce
défaut pour divulguer des informations, provoquer un déni de service ou
interférer avec la session VNC d'un autre utilisateur sur l'hôte.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.9.13-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets x11vnc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de x11vnc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/x11vnc">https://security-tracker.debian.org/tracker/x11vnc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2490.data"
# $Id: $
