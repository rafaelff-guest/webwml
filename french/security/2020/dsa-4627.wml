#use wml::debian::translation-check translation="0f2e63875207f63029067f8878524f544b09a20f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3862">CVE-2020-3862</a>

<p>Srikanth Gatta a découvert qu'un site web malveillant peut être capable
de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3864">CVE-2020-3864</a>

<p>Ryan Pickren a découvert qu'un contexte d'objet DOM peut ne pas avoir eu
une unique origine de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3865">CVE-2020-3865</a>

<p>Ryan Pickren a découvert qu'un contexte d'objet DOM de premier niveau
peut avoir été considéré comme sûr de façon incorrecte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3867">CVE-2020-3867</a>

<p>Un chercheur anonyme a découvert que le traitement de contenus web
contrefaits pourrait conduire à un script intersite universel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3868">CVE-2020-3868</a>

<p>Marcin Towalski a découvert que le traitement de contenus web
contrefaits pourrait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.26.4-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4627.data"
# $Id: $
