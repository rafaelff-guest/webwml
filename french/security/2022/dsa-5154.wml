#use wml::debian::translation-check translation="97c27f996b83d25078a910a4ed9e19cae7e68487" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26700">CVE-2022-26700</a>

<p>ryuzaki a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26709">CVE-2022-26709</a>

<p>Chijin Zhou a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26716">CVE-2022-26716</a>

<p>SorryMybad a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26717">CVE-2022-26717</a>

<p>Jeonghoon Shin a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26719">CVE-2022-26719</a>

<p>Dongzhuo Zhao a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30293">CVE-2022-30293</a>

<p>Chijin Zhou a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire ou à un déni de service
(plantage d'application).</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 2.36.3-1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.36.3-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5154.data"
# $Id: $
